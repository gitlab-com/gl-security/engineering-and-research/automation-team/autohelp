import json
from urllib.parse import urlparse

from pytest import fixture
from pytest import skip
from pytest import mark

from gitlab.const import AccessLevel

from autohelp import logging
from autohelp.gitlab import (
    GitLabAdminHelper,
    GitLabGraphQLAdminHelper,
    GitLabHelper,
    ISSUE_URL_RE,
)

log = logging.getLogger(__name__)

VALID_ISSUE_URLS = [
    'https://gitlab.com/my-group/my-project/issues/12345',
    'https://gitlab.com/my-group/my-project/-/issues/12345',
    '/my-group/my-project/-/issues/12345',
    'my-group/my-project/-/issues/12345',
]

INVALID_ISSUE_URLS = [
    'https://gitlab.com/my-group/my-project/-/issues',
    'https://gitlab.com/my-group/my-project',
    '12345',
    'Hi!',
]

# so long as flatten_members_map and get_members_map are tested
# against staging.gitlab.com, a token will be required for
# that instance. Either GITLAB_ADMIN_TOKEN_staging_gitlab_com or
# GITLAB_TOKEN_staging_gitlab_com works as long as the latter
# belongs to the account owning
# https://staging.gitlab.com/secauto-test-hierarchy

_GITLAB_INSTANCE = 'staging.gitlab.com'


def test_issue_url_valid():
    """Test ISSUE_URL_RE with valid input."""
    for url in VALID_ISSUE_URLS:
        match = ISSUE_URL_RE.match(url)
        assert match is not None, f'Should match: {url}'
        assert match.group(1) == 'my-group/my-project'
        assert match.group(2) == '12345'


def test_issue_url_invalid():
    """Test ISSUE_URL_RE with invalid input."""
    for url in INVALID_ISSUE_URLS:
        match = ISSUE_URL_RE.match(url)
        assert match is None, f'Should not match: {url}'


def test_parse_attachments():
    """Test parse_attachments."""
    text = """
    An attachment: [test.zip](/uploads/abc123/test.zip)
    Another one: ![test.png](/uploads/def456/test.png)
    """
    result = GitLabHelper.parse_attachments(text)
    assert len(result) == 2
    assert result.get('test.zip') == '/uploads/abc123/test.zip'
    assert result.get('test.png') == '/uploads/def456/test.png'


def test_parse_attachments_empty():
    """Test parse_attachments without attachments."""
    text = 'A comment without attachments.'
    result = GitLabHelper.parse_attachments(text)
    assert len(result) == 0


def get_serialized_member_map(mmap):
    """
    Auxiliary function which returns the serializable version
    of a members map
    """
    nmmap = {}

    if 'direct' in mmap:
        new_direct = []
        direct_serialized = all([type(e) is str for e in mmap['direct']])
        if not direct_serialized:
            for e in mmap['direct']:
                new_direct.append(f'{e.username}@{e.access_level}')
            nmmap['direct'] = new_direct

    if 'shared_with_access' in mmap:
        nmmap['shared_with_access'] = mmap['shared_with_access']

    for k, v in mmap.items():
        if isinstance(v, dict):
            nmmap[k] = get_serialized_member_map(v)

    return nmmap


def get_serialized_member_list(mlist):
    """
    Auxiliary version which returns the serializable
    version of a members list
    """
    nmlist = []

    for m, sp in mlist:
        nm = [f'{m.username}@{m.access_level}', sp]
        nmlist.append(nm)
    return nmlist


@fixture(scope='module')
def G():
    """Fixture for tests requiring a GitLabAdminHelper instance"""
    G = GitLabAdminHelper(_GITLAB_INSTANCE)
    return G


@fixture(scope='module')
def GQL():
    """Fixture for tests requiring a GitLabGraphQLAdminHelper instance"""
    GQL = GitLabGraphQLAdminHelper(_GITLAB_INSTANCE)
    return GQL


@fixture(
    scope='module',
    params=[
        {
            'type': 'project',
            'url': 'https://staging.gitlab.com/secauto-test-hierarchy/top/subgroup/project',  # noqa
        },
        {
            'type': 'group',
            'url': 'https://staging.gitlab.com/secauto-test-hierarchy/top/subgroup',  # noqa
        },
    ],
)
def members_map(G, request):
    """Fixture which returns a url and members map of a resource"""
    rtype = request.param['type']
    rurl = request.param['url']
    rpath = urlparse(rurl).path

    if rtype == 'project':
        res = G.get_project(rpath[1:])
    else:
        res = G.get_group(rpath[1:])

    rurl, rmmap = G.get_members_map(res)
    return rurl, rmmap


@fixture(
    scope='module',
    params=[
        {
            'url': 'https://staging.gitlab.com/secauto-test-hierarchy/top/subgroup/project',  # noqa
            'file': './tests/fixture_gitlab_get_members_map_project.json',
        },
        {
            'url': 'https://staging.gitlab.com/secauto-test-hierarchy/top/subgroup',  # noqa
            'file': './tests/fixture_gitlab_get_members_map_group.json',
        },
    ],
)
def serialized_members_map(request):
    """
    Parametrized fixture that returns a url and members map of a
    project from a file
    """
    with open(request.param['file'], 'r') as f:
        return request.param['url'], json.loads(f.read())


@fixture(
    scope='module',
    params=[
        {
            'url': 'https://staging.gitlab.com/secauto-test-hierarchy/top/subgroup/project',  # noqa
            'include_all_paths': False,
            'file': './tests/fixture_gitlab_flatten_members_map_project_false.json',  # noqa
        },
        {
            'url': 'https://staging.gitlab.com/secauto-test-hierarchy/top/subgroup/project',  # noqa
            'include_all_paths': True,
            'file': './tests/fixture_gitlab_flatten_members_map_project_true.json',  # noqa
        },
        {
            'url': 'https://staging.gitlab.com/secauto-test-hierarchy/top/subgroup',  # noqa
            'include_all_paths': False,
            'file': './tests/fixture_gitlab_flatten_members_map_group_false.json',  # noqa
        },
        {
            'url': 'https://staging.gitlab.com/secauto-test-hierarchy/top/subgroup',  # noqa
            'include_all_paths': True,
            'file': './tests/fixture_gitlab_flatten_members_map_group_true.json',  # noqa
        },
    ],
)
def serialized_access_paths(request):
    """
    Parametrized fixture that returns a url and members list
    of a project from a file
    """
    with open(request.param['file'], 'r') as f:
        return (
            request.param['url'],
            request.param['include_all_paths'],
            json.loads(f.read()),
        )


@mark.integration
def test_get_members_map(G, members_map, serialized_members_map):
    """
    Tests that the members map for a url is equal to a serialized version.
    """
    rurl, rmmap = members_map
    furl, fmmap = serialized_members_map
    smmap = get_serialized_member_map(rmmap)

    if rurl != furl:
        skip(f'Invalid case: {rurl} != {furl}')

    assert smmap == fmmap


@mark.integration
def test_get_breadcrumb_urls(G):
    furl = 'https://gitlab.com/group/subgroup/project'
    fbcs = ['https://gitlab.com/group', 'https://gitlab.com/group/subgroup']
    r = G._get_breadcrumb_urls(furl)
    assert r == fbcs


@mark.integration
def test_flatten_members_map(
    G,
    members_map,
    serialized_access_paths,
):
    rurl, rmmap = members_map
    furl, finclude_all_paths, fmlist = serialized_access_paths

    if rurl != furl:
        skip(f'Invalid case: {rurl} != {furl}')

    rmlist = G._flatten_members_map(rmmap, finclude_all_paths)
    smlist = get_serialized_member_list(rmlist)
    assert smlist == fmlist


@mark.integration
def test_gql_get_users_info(GQL):
    sa0info = {'https://staging.gitlab.com/secautotest0': ('secautotest0', ['security-automation+staging+secautotest0@gitlab.com'])}

    assert sa0info == GQL.get_users_info('secautotest0')

    sa4info = {
        'https://staging.gitlab.com/secautotest4': (
            'secautotest4',
            ['security-automation+staging+secautotest4@gitlab.com'],
        )
    }

    assert sa4info == GQL.get_users_info('secautotest4')


@mark.integration
def create_test_hierarchy():
    G = GitLabAdminHelper(_GITLAB_INSTANCE)
    c = G.client

    prefix = 'secautotest'
    users = []
    for i in range(0, 9):
        uid = f'{prefix}{i}'
        email = f'security-automation+staging+{uid}@gitlab.com'
        try:
            nu = c.users.list(username=uid)[0]
        except Exception as e:
            nu = c.users.create(
                {
                    'email': email,
                    'username': uid,
                    'force_random_password': 'true',
                    'name': uid,
                    'skip_confirmation': True,
                }
            )
        users.append(nu)

    u0, u1 = users[0], users[1]
    u2, u3 = users[2], users[3]
    u4, u5 = users[4], users[5]
    u6, u7 = users[6], users[7]
    u8 = users[8]

    # root group must be first removed and then created manually
    # https://staging.gitlab.com/groups/new#create-group-pane
    # and put in a paid plan in order to have more than 5 members
    # https://staging.gitlab.com/admin/groups/secauto-test-hierarchy/edit

    root = c.groups.get('secauto-test-hierarchy')
    root.members.create({'user_id': u4.id, 'access_level': AccessLevel.OWNER})

    c.auth()
    cuser = c.user
    root.members.delete(cuser.id)

    gm = c.groups.create({'name': 'top', 'path': 'top', 'parent_id': root.id})
    gm.members.create({'user_id': u0.id, 'access_level': AccessLevel.MAINTAINER})  # noqa
    gm.members.create({'user_id': u1.id, 'access_level': AccessLevel.GUEST})
    gm.members.create({'user_id': u2.id, 'access_level': AccessLevel.MAINTAINER})  # noqa
    gm.members.create({'user_id': u3.id, 'access_level': AccessLevel.MAINTAINER})  # noqa
    gm.members.create({'user_id': u4.id, 'access_level': AccessLevel.OWNER})

    g1 = c.groups.create({'name': 'g1', 'path': 'g1', 'parent_id': root.id})
    g1.members.create({'user_id': u5.id, 'access_level': AccessLevel.OWNER})
    g1.members.create({'user_id': u6.id, 'access_level': AccessLevel.OWNER})
    g1.members.create({'user_id': u1.id, 'access_level': AccessLevel.OWNER})
    g1.members.create({'user_id': u3.id, 'access_level': AccessLevel.REPORTER})
    gm.share(g1.id, AccessLevel.OWNER)

    g2 = c.groups.create({'name': 'g2', 'path': 'g2', 'parent_id': root.id})
    g2.members.create({'user_id': u5.id, 'access_level': AccessLevel.OWNER})
    g2.members.create({'user_id': u6.id, 'access_level': AccessLevel.OWNER})
    g2.members.create({'user_id': u1.id, 'access_level': AccessLevel.OWNER})
    gm.share(g2.id, AccessLevel.MAINTAINER)

    g3 = c.groups.create({'name': 'g3', 'path': 'g3', 'parent_id': root.id})
    g3.members.create({'user_id': u5.id, 'access_level': AccessLevel.OWNER})
    g3.members.create({'user_id': u6.id, 'access_level': AccessLevel.OWNER})
    g3.members.create({'user_id': u1.id, 'access_level': AccessLevel.OWNER})
    g3.members.create({'user_id': u8.id, 'access_level': AccessLevel.MAINTAINER})  # noqa

    g1_g1 = c.groups.create({'name': 'g1', 'path': 'g1', 'parent_id': g1.id})

    g1_g2 = c.groups.create({'name': 'g2', 'path': 'g2', 'parent_id': g1.id})
    g1_g2.members.create({'user_id': u7.id, 'access_level': AccessLevel.OWNER})  # noqa
    g1_g1.share(g1_g2.id, AccessLevel.OWNER)

    g1_g3 = c.groups.create({'name': 'g3', 'path': 'g3', 'parent_id': g1.id})
    g1_g3.members.create({'user_id': u4.id, 'access_level': AccessLevel.OWNER})

    gm_sg = c.groups.create(
        {'name': 'subgroup', 'path': 'subgroup', 'parent_id': gm.id}
    )
    gm_sg.share(g1_g3.id, AccessLevel.REPORTER)

    gm_sg_p0 = c.projects.create({'name': 'project', 'namespace_id': gm_sg.id})
    gm_sg_p0.share(g1_g1.id, AccessLevel.REPORTER)
    gm_sg_p0.share(g1_g3.id, AccessLevel.MAINTAINER)
    gm_sg_p0.share(g3.id, AccessLevel.REPORTER)


def main():
    """
    Running test_gitlab.py creates the serialized member maps
    used by these tests create_test_hierarchy can be used if
    the hierarchy(ies) in question don't exist yet.
    """

    G = GitLabAdminHelper(_GITLAB_INSTANCE)

    p = G.get_project('secauto-test-hierarchy/top/subgroup/project')

    _, pmmap = G.get_members_map(p)
    spmmap = get_serialized_member_map(pmmap)
    with open('./tests/fixture_gitlab_get_members_map_project.json', 'w+') as f:  # noqa
        f.write(json.dumps(spmmap, indent=4))

    pmlistall = G._flatten_members_map(pmmap, True)
    spmlistall = get_serialized_member_list(pmlistall)
    with open(
        './tests/fixture_gitlab_flatten_members_map_project_true.json', 'w+'
    ) as f:  # noqa
        f.write(json.dumps(spmlistall, indent=4))

    pmlistmax = G._flatten_members_map(pmmap, False)
    spmlistmax = get_serialized_member_list(pmlistmax)
    with open(
        './tests/fixture_gitlab_flatten_members_map_project_false.json', 'w+'
    ) as f:  # noqa
        f.write(json.dumps(spmlistmax, indent=4))

    g = G.get_group('secauto-test-hierarchy/top/subgroup')

    _, gmmap = G.get_members_map(g)
    sgmmap = get_serialized_member_map(gmmap)
    with open('./tests/fixture_gitlab_get_members_map_group.json', 'w+') as f:
        f.write(json.dumps(sgmmap, indent=4))

    gmlistall = G._flatten_members_map(gmmap, True)
    sgmlistall = get_serialized_member_list(gmlistall)
    with open(
        './tests/fixture_gitlab_flatten_members_map_group_true.json', 'w+'
    ) as f:  # noqa
        f.write(json.dumps(sgmlistall, indent=4))

    gmlistmax = G._flatten_members_map(gmmap, False)
    sgmlistmax = get_serialized_member_list(gmlistmax)
    with open(
        './tests/fixture_gitlab_flatten_members_map_group_false.json', 'w+'
    ) as f:  # noqa
        f.write(json.dumps(sgmlistmax, indent=4))

    return


if __name__ == "__main__":
    main()
