from autohelp.pagerduty import ID_RE


VALID_IDS = [
    'PABC123',
    'P4D5E6F'
]

INVALID_IDS = [
    'test@example.com',
    'hello_world',
    'PABC/../123',
]


def test_id_valid():
    """Test ID_RE with valid input."""
    for id_ in VALID_IDS:
        match = ID_RE.match(id_)
        assert match is not None, f'Should match: {id_}'


def test_id_invalid():
    """Test ID_RE with invalid input."""
    for id_ in INVALID_IDS:
        match = ID_RE.match(id_)
        assert match is None, f'Should not match: {id_}'
