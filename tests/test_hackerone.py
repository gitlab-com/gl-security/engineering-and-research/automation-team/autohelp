from autohelp.hackerone import REPORT_ID_RE


VALID_REPORTS = [
    'https://hackerone.com/reports/123456',
    '123456',
]

INVALID_REPORTS = [
    'https://hackerone.com/reports/',
    'https://hackerone.com/',
    'Hi!',
]


def test_report_id_valid():
    """Test REPORT_ID_RE with valid input."""
    for report in VALID_REPORTS:
        match = REPORT_ID_RE.match(report)
        assert match is not None, f'Should match: {report}'
        assert match.group(1) == '123456'


def test_report_id_invalid():
    """Test REPORT_ID_RE with invalid input."""
    for report in INVALID_REPORTS:
        match = REPORT_ID_RE.match(report)
        assert match is None, f'Should not match: {report}'
