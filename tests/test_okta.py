from autohelp.okta import ID_RE


VALID_IDS = [
    '012345abcdefABCDEF',
]

INVALID_IDS = [
    'test@example.com',
    'hello_world',
    '01234abcd/../foo',
]


def test_id_valid():
    """Test ID_RE with valid input."""
    for id_ in VALID_IDS:
        match = ID_RE.match(id_)
        assert match is not None, f'Should match: {id_}'


def test_id_invalid():
    """Test ID_RE with invalid input."""
    for id_ in INVALID_IDS:
        match = ID_RE.match(id_)
        assert match is None, f'Should not match: {id_}'
