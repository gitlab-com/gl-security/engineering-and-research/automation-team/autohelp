import datetime as dt

import pytest

from autohelp.click import DayOrAgeDate, DayOrAgeDatetime


class TestDayOrAgeDate():
    def test_date(self):
        """Test with a date."""
        doa = DayOrAgeDate('1970-01-01')
        assert isinstance(doa, dt.date)
        assert doa.year == 1970
        assert doa.month == 1
        assert doa.day == 1

    def test_number(self):
        """Test with a number."""
        expected = dt.date.today() - dt.timedelta(days=7)
        doa = DayOrAgeDate('7')
        assert isinstance(doa, dt.date)
        assert doa == expected

    def test_datetime_date(self):
        """Test with a datetime.date."""
        doa = DayOrAgeDate(dt.date(1970, 1, 1))
        assert isinstance(doa, dt.date)
        assert doa.year == 1970
        assert doa.month == 1
        assert doa.day == 1

    def test_none(self):
        """Test with None."""
        doa = DayOrAgeDate(None)
        assert doa is None

    def test_invalid(self):
        """Test with an invalid value."""
        with pytest.raises(Exception):
            DayOrAgeDate('Hi!')


class TestDayOrAgeDatetime():
    def test_date(self):
        """Test with a date."""
        doa = DayOrAgeDatetime('1970-01-01')
        assert isinstance(doa, dt.datetime)
        assert doa.year == 1970
        assert doa.month == 1
        assert doa.day == 1

    def test_number(self):
        """Test with a number."""
        expected = dt.datetime.now(dt.timezone.utc) - dt.timedelta(hours=7)
        doa = DayOrAgeDatetime('7')
        assert isinstance(doa, dt.datetime)
        assert (doa - expected).total_seconds() < 1

    def test_datetime_datetime(self):
        """Test with a datetime.datetime."""
        doa = DayOrAgeDatetime(dt.datetime(1970, 1, 1))
        assert isinstance(doa, dt.datetime)
        assert doa.year == 1970
        assert doa.month == 1
        assert doa.day == 1

    def test_none(self):
        """Test with None."""
        doa = DayOrAgeDatetime(None)
        assert doa is None

    def test_invalid(self):
        """Test with an invalid value."""
        with pytest.raises(Exception):
            DayOrAgeDatetime('Hi!')
