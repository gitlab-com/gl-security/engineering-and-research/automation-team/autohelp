
import pytest


def pytest_addoption(parser):
    parser.addoption(
        "--include-integration",
        action="store_true",
        default=False,
        help="Also run integration tests",
    )


def pytest_collection_modifyitems(config, items):
    if not config.getoption("--include-integration"):
        skipper = pytest.mark.skip(reason="Only run when --include-integration is given")
        for item in items:
            if "integration" in item.keywords:
                item.add_marker(skipper)
