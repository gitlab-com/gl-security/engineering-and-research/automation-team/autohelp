import os

from phonebook_client import AuthenticatedClient
from phonebook_client.api.members import get_group_member
from phonebook_client.api.users import get_user
from phonebook_client.models import AccessLevel, Member, User, UserKey


API_URL = 'https://phonebook.primary.secauto-live.sec.gitlab.net'


class PhonebookHelper:
    """Wrapper for generated Phonebook client."""

    def __init__(self, api_token=None, url=API_URL):
        api_token = os.environ.get('PHONEBOOK_API_TOKEN', api_token)
        if api_token is None:
            raise Exception('Phonebook API token missing.')
        self.client = AuthenticatedClient(base_url=url, token=api_token)

    def get_group_member(self, group_id, user_id):
        """Return the GitLab group member, or None."""
        res = get_group_member.sync_detailed(
            client=self.client,
            group_id=group_id,
            user_id=user_id)
        if res.status_code == 200:
            return res.parsed
        elif res.status_code == 404:
            return None
        else:
            raise Exception(f'Phonebook API response: {res.status_code}')

    def get_user(self, key, value):
        """Return the single matching User, or None."""
        res = get_user.sync_detailed(client=self.client, k=key, v=value)
        if res.status_code == 200:
            return res.parsed
        elif res.status_code == 404:
            return None
        else:
            raise Exception(f'Phonebook API response: {res.status_code}')
