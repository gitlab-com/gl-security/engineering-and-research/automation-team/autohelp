import os
import re

import requests


INCIDENTS_API = 'https://api.pagerduty.com/incidents'
USERS_API = 'https://api.pagerduty.com/users'

ID_RE = re.compile(r'^[0-9A-Z]+$')

TIMEOUT = 10


class PagerDutyHelper():
    """PagerDuty API client.

    See: https://developer.pagerduty.com/api-reference/
    """

    def __init__(self, api_token=None):
        api_token = os.environ.get('PAGERDUTY_API_TOKEN', api_token)
        if api_token is None:
            raise Exception('PagerDuty API token missing.')
        self.api_token = api_token

    def create_incident(self, service_id, email, title, body, key=None):
        """Create and return a new Incident.

        See: https://developer.pagerduty.com/api-reference/reference/REST/openapiv3.json/paths/~1incidents/post
        """  # noqa
        headers = {
            'Accept': 'application/vnd.pagerduty+json;version=2',
            'Authorization': 'Token token=' + self.api_token,
            'From': email,
        }
        data = {
            'incident': {
                'type': 'incident',
                'title': title,
                'body': {
                    'details': body,
                    'type': 'incident_body'
                },
                'service': {
                    'id': service_id,
                    'type': 'service_reference'
                }
            }
        }
        if key is not None:
            data['incident']['incident_key'] = key
        res = requests.post(
            INCIDENTS_API,
            headers=headers,
            json=data,
            timeout=TIMEOUT)
        res.raise_for_status()
        return Incident(res.json()['incident'])

    def find_incidents(self, service_id, incident_key=None):
        """Iterate over matching Incidents."""
        query = {
            'service_ids[]': service_id,
            'date_range': 'all',
        }
        if incident_key is not None:
            query['incident_key'] = incident_key
        return self._list(INCIDENTS_API, 'incidents', Incident, query=query)

    def get_user(self, user_id):
        """Return the User, or None."""
        self._check_id(user_id)
        url = f'{USERS_API}/{user_id}'
        return self._get(url, 'user', User)

    def list_users(self):
        """Iterate over all Users."""
        return self._list(USERS_API, 'users', User)

    def _get(self, url, entity_key, entity_type, query=None):
        """Return the resource, or None."""
        headers = {
            'Accept': 'application/vnd.pagerduty+json;version=2',
            'Authorization': 'Token token=' + self.api_token,
        }
        res = requests.get(
            url,
            headers=headers,
            params=query,
            timeout=TIMEOUT)
        if res.status_code == 200:
            return entity_type(res.json()[entity_key])
        elif res.status_code == 404:
            return None
        else:
            raise Exception(f'PagerDuty API Error: {res.text}')

    def _list(self, url, entity_key, entity_type, query=None):
        """Iterate over matching resources."""
        headers = {
            'Accept': 'application/vnd.pagerduty+json;version=2',
            'Authorization': 'Token token=' + self.api_token,
        }
        params = {
            'limit': 50,
            'offset': 0,
        }
        if query is not None:
            params.update(query)
        entities = []
        while url or entities:
            if entities:
                yield entity_type(entities.pop(0))
            else:
                res = requests.get(
                    url,
                    headers=headers,
                    params=params,
                    timeout=TIMEOUT)
                res.raise_for_status()
                body = res.json()
                entities = body.get(entity_key)
                more = body.get('more')
                if more:
                    params['offset'] += params['limit']
                else:
                    url = None

    @staticmethod
    def _check_id(value):
        """Check if the value is a valid ID."""
        if (m := ID_RE.match(value)) is None:
            raise Exception(f'Invalid ID: {value}')


class Incident():
    """PagerDuty Incident.

    See: https://developer.pagerduty.com/api-reference/reference/REST/openapiv3.json/paths/~1incidents~1%7Bid%7D/get
    """  # noqa

    def __init__(self, data):
        self.data = data

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.number} ({self.status})>'

    @property
    def assignee(self):
        """Return the last assigned Agent, or None."""
        if len(self.data['assignments']) > 0:
            return Agent(self.data['assignments'][-1]['assignee'])
        else:
            return None

    @property
    def key(self):
        """Return the incident key."""
        return self.data['incident_key']

    @property
    def last_changer(self):
        """Return the Agent causing the last status change, or None."""
        if 'last_status_change_by' in self.data:
            return Agent(self.data['last_status_change_by'])
        else:
            return None

    @property
    def number(self):
        """Return the incident number."""
        return self.data['incident_number']

    @property
    def status(self):
        """Return the incident status."""
        return self.data['status']

    @property
    def web_url(self):
        """Return the incident's web URL."""
        return self.data['html_url']


class Agent():
    """PagerDuty Agent.

    See: https://developer.pagerduty.com/api-reference/reference/REST/openapiv3.json/components/schemas/AgentReference
    """  # noqa

    def __init__(self, data):
        self.data = data

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.id} ({self.summary})>'

    @property
    def id(self):
        """Return the agent's ID."""
        return self.data['id']

    @property
    def summary(self):
        """Return the agent's summary."""
        return self.data['summary']


class User():
    """PagerDuty User.

    See: https://developer.pagerduty.com/api-reference/reference/REST/openapiv3.json/paths/~1users/get
    """  # noqa

    def __init__(self, data):
        self.data = data

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.id} ({self.name})>'

    @property
    def id(self):
        """Return the user's ID."""
        return self.data['id']

    @property
    def email(self):
        """Return the user's email address."""
        return self.data['email']

    @property
    def name(self):
        """Return the user's name."""
        return self.data['name']
