import datetime as dt
import re

import click


# YYYY-MM-DD or N+
_DAY_OR_NUMBER_RE = re.compile(r'^(?:(\d{4})-(\d\d)-(\d\d)|(\d+))$')


class DayOrAgeDateType(click.ParamType):
    """A datetime.date specified as "YYYY-MM-DD" or "N" (days ago)."""
    name = 'day_or_age_date'

    def convert(self, value, param, ctx):
        if isinstance(value, dt.date):
            return value
        numbers = _day_or_number(value)
        if len(numbers) == 3:
            return dt.date(*numbers)
        elif len(numbers) == 1:
            return dt.date.today() - dt.timedelta(days=numbers[0])
        else:
            self.fail(f'Bad value: {value}', param, ctx)


# DEPRECATED Please use DayOrAgeDate
DateOrAge = DayOrAgeDateType()
# A datetime.date specified as "YYYY-MM-DD" or "N" (days ago).
DayOrAgeDate = DayOrAgeDateType()


class DayOrAgeDatetimeType(click.ParamType):
    """A UTC datetime.datetime specified as "YYYY-MM-DD" or "N" (hours ago)."""
    name = 'day_or_age_datetime'

    def convert(self, value, param, ctx):
        if isinstance(value, dt.datetime):
            return value
        numbers = _day_or_number(value)
        if len(numbers) == 3:
            return dt.datetime(*numbers, tzinfo=dt.timezone.utc)
        elif len(numbers) == 1:
            return dt.datetime.now(dt.timezone.utc) - dt.timedelta(hours=numbers[0])
        else:
            self.fail(f'Bad value: {value}', param, ctx)


# A UTC datetime.datetime specified as "YYYY-MM-DD" or "N" (hours ago).
DayOrAgeDatetime = DayOrAgeDatetimeType()


def fail(message, status=1):
    """Exit after printing the error message."""
    click.echo(f'ERROR: {message}', err=True)
    click.get_current_context().exit(status)


def _day_or_number(value):
    """Return a list of three, one, or no ints, depending on the match."""
    m = _DAY_OR_NUMBER_RE.match(value)
    if m is None:
        return []
    groups = m.groups()
    if groups[0] is not None:
        return [int(groups[0]), int(groups[1]), int(groups[2])]
    else:
        return [int(groups[3])]
