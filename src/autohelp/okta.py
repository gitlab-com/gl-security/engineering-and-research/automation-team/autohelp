import os
import re

import dateutil.parser as dp
import requests


DEFAULT_INSTANCE = 'gitlab.okta.com'
OKTA_API_APPS = '/api/v1/apps'
OKTA_API_USERS = '/api/v1/users'

ID_RE = re.compile(r'^[0-9A-Za-z]+$')
NEXT_LINK_RE = re.compile(r'<(.+)>.*;.*rel="?next"?.*')
TIMEOUT = 10


class OktaAPIHelper:
    """Generic Okta API client.

    See: https://developer.okta.com/docs/reference/core-okta-api/
    """

    def __init__(self, url, api_token=None):
        self.url = url
        self.api_token = os.environ.get('OKTA_API_TOKEN', api_token)
        if self.api_token is None:
            raise Exception('Okta API token missing.')

    def _get(self, resource, params=None, entity_type=None):
        """Return the resource, or None."""
        url = self.url + resource
        headers = {
            'Accept': 'application/json',
            'Authorization': f'SSWS {self.api_token}',
        }
        res = requests.get(
            url,
            params=params,
            headers=headers,
            timeout=TIMEOUT)
        if res.status_code == 404:
            return None
        else:
            res.raise_for_status()
        if entity_type is None:
            return res.json()
        else:
            return entity_type(res.json())

    def _list(self, resource=None, params=None, entity_type=None):
        """Iterate over matching resources."""
        url = self.url if resource is None else self.url + resource
        headers = {
            'Accept': 'application/json',
            'Authorization': f'SSWS {self.api_token}',
        }
        entities = []
        while url or entities:
            if entities:
                if entity_type is None:
                    yield entities.pop(0)
                else:
                    yield entity_type(entities.pop(0))
            else:
                res = requests.get(
                    url,
                    params=params,
                    headers=headers,
                    timeout=TIMEOUT)
                res.raise_for_status()
                url = self._get_next_link(res.headers)
                params = None
                entities = res.json()

    def _post(self, resource, data=None, params=None, entity_type=None):
        """POST the resource."""
        url = self.url + resource
        headers = {
            'Accept': 'application/json',
            'Authorization': f'SSWS {self.api_token}',
        }
        res = requests.post(
            url,
            params=params,
            headers=headers,
            json=data,
            timeout=TIMEOUT)
        res.raise_for_status()
        if entity_type is None:
            return res.json()
        else:
            return entity_type(res.json())

    def _put(self, resource, data=None, params=None, entity_type=None):
        """PUT the resource."""
        url = self.url + resource
        headers = {
            'Accept': 'application/json',
            'Authorization': f'SSWS {self.api_token}',
        }
        res = requests.put(
            url,
            params=params,
            headers=headers,
            json=data,
            timeout=TIMEOUT)
        res.raise_for_status()
        if entity_type is None:
            return res.json()
        else:
            return entity_type(res.json())

    @staticmethod
    def _get_next_link(headers):
        """Parse the rel=next Link header returned by Okta."""
        header = headers.get('Link', '')
        for link in [lnk.strip() for lnk in header.split(',')]:
            if (m := NEXT_LINK_RE.match(link)) is not None:
                return m.group(1)
        else:
            return None

    @staticmethod
    def _check_id(value):
        """Check if the value is a valid ID."""
        if (m := ID_RE.match(value)) is None:
            raise Exception(f'Invalid ID: {value}')


class OktaAppsHelper(OktaAPIHelper):
    """Okta Apps API client.

    See: https://developer.okta.com/docs/reference/api/apps/
    """

    def __init__(self, api_token=None, instance=DEFAULT_INSTANCE):
        url = 'https://' + instance + OKTA_API_APPS
        super().__init__(url, api_token=api_token)

    def list_users(self, app_id, params=None):
        """Iterate over matching AppUsers."""
        self._check_id(app_id)
        resource = f'/{app_id}/users'
        if params is None:
            params = {}
        else:
            params = dict(params)
        params['limit'] = 500
        return self._list(
            resource=resource,
            params=params,
            entity_type=AppUser)

    def get_user(self, app_id, user_id):
        """Return the AppUser, or None."""
        self._check_id(app_id)
        self._check_id(user_id)
        resource = f'/{app_id}/users/{user_id}'
        return self._get(resource, entity_type=AppUser)

    def update_user(self, app_id, user):
        """Update the AppUser profile."""
        self._check_id(app_id)
        resource = f'/{app_id}/users/{user.id}'
        return self._post(resource, data={'profile': user.profile})


class OktaUsersHelper(OktaAPIHelper):
    """Okta Users API client.

    See: https://developer.okta.com/docs/reference/api/users/
    """

    def __init__(self, api_token=None, instance=DEFAULT_INSTANCE):
        url = 'https://' + instance + OKTA_API_USERS
        super().__init__(url, api_token=api_token)

    def list(self, params=None):
        """Iterate over matching Users."""
        return self._list(params=params, entity_type=User)

    def list_one(self, params=None):
        """Return the single matching User, or None."""
        users = list(self._list(params=params, entity_type=User))
        return users[0] if len(users) == 1 else None

    def get(self, user_id):
        """Return the User, or None."""
        self._check_id(user_id)
        resource = f'/{user_id}'
        return self._get(resource, entity_type=User)

    def get_by_email(self, email):
        """Return the single matching User, or None."""
        params = {'search': f'profile.email eq "{email}" or profile.secondEmail eq "{email}" or profile.gsuite_emailAliases eq "{email}"'}  # noqa
        users = list(self.list(params))
        result = None
        if len(users) == 1:
            result = users[0]
        else:
            for user in users:
                if user.email == email:
                    result = user
                    break
        return result

    def get_by_profile(self, key, value):
        """Return the single matching User, or None."""
        params = {'search': f'profile.{key} eq "{value}"'}
        return self.list_one(params)

    def update_partial(self, okta_user, properties):
        """Update the User profile partially."""
        resource = f'/{okta_user.id}'
        return self._post(
            resource,
            data={'profile': properties},
            entity_type=User)

    def update_strict(self, okta_user):
        """Update the User profile completely."""
        resource = f'/{okta_user.id}'
        return self._put(
            resource,
            data={'profile': okta_user.profile},
            entity_type=User)


class AppUser:
    """Application user information.

    See: https://developer.okta.com/docs/reference/api/apps/#application-user-object
    """  # noqa

    def __init__(self, data):
        self.data = data
        self.profile = data.get('profile', {})

    @property
    def external_id(self):
        """Return the user ID in the app, or None."""
        return self.data.get('externalId')

    @property
    def id(self):
        """Return the Okta app user ID."""
        return self.data['id']

    @property
    def status(self):
        """Return the Okta app user status."""
        return self.data['status']

    @property
    def username(self):
        """Return the username in the app, or None."""
        return self.data.get('credentials', {}).get('userName')

    def get(self, name):
        """Return the profile attribute, or None."""
        return self.profile.get(name)

    def set(self, name, value):
        """Set the profile attribute value."""
        self.profile[name] = value


class User:
    """User information.

    See: https://developer.okta.com/docs/reference/api/users/#user-object
    """

    def __init__(self, data):
        self.data = data
        self.profile = data.get('profile', {})

    def __repr__(self):
        return f'<User {self.id} ({self.display_name})>'

    @property
    def created(self):
        """Return the Okta user creation datetime."""
        return dp.parse(self.data['created'])

    @property
    def country_code(self):
        """Return the country code, or None."""
        return self.profile.get('countryCode')

    @property
    def department(self):
        """Return the "department" base attribute, or None."""
        return self.profile.get('department')

    @property
    def display_name(self):
        """Return the "displayName" base attribute, or None."""
        return self.profile.get('displayName')

    @property
    def email(self):
        """Return the "email" base attribute, or None."""
        return self.profile.get('email')

    @property
    def email_aliases(self):
        """Return the G Suite email aliases, or an empty list."""
        return self.profile.get('gsuite_emailAliases', [])

    @property
    def emails(self):
        """Return a list of all known email addresses."""
        result = [self.email, self.second_email, *self.email_aliases]
        return list(filter(None, result))

    @property
    def employee_number(self):
        """Return the "employeeNumber" base attribute, or None."""
        return self.profile.get('employeeNumber')

    @property
    def first_name(self):
        """Return the "firstName" base attribute, or None."""
        return self.profile.get('firstName')

    @property
    def id(self):
        """Return the Okta user ID."""
        return self.data['id']

    @property
    def last_login(self):
        """Return the Okta user last login datetime, or None."""
        value = self.data.get('lastLogin')
        return dp.parse(value) if value else None

    @property
    def location(self):
        """Return the city and state, if any."""
        city = self.profile.get('city')
        state = self.profile.get('state')
        return ', '.join(filter(None, [city, state]))

    @property
    def manager(self):
        """Return the "manager" base attribute, or None."""
        return self.profile.get('manager')

    @property
    def manager_id(self):
        """Return the "managerId" base attribute, or None."""
        result = self.profile.get('managerId')
        return result if result else None

    @property
    def role(self):
        """Return the job title, or None."""
        return self.profile.get('title')

    @property
    def second_email(self):
        """Return the "secondEmail" base attribute, or None."""
        return self.profile.get('secondEmail')

    @property
    def status(self):
        """Return the Okta user status."""
        return self.data['status']

    def get(self, key):
        """Return the profile attribute, or None."""
        return self.profile.get(key)
