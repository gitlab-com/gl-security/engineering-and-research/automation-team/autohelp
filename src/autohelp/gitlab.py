import gitlab
from gitlab.v4.objects.groups import Group
from gitlab.v4.objects.projects import Project
from gitlab.exceptions import GitlabGetError

from graphql import DocumentNode
from gql import gql, Client
from gql.transport.requests import RequestsHTTPTransport

import dateutil.parser as dp
import requests
from urllib.parse import urlparse

from collections import defaultdict
from collections import namedtuple

from functools import reduce

import datetime as dt
import os
import re
import copy

import logging

log = logging.getLogger(__name__)


GITLAB_USER_AGENT = os.environ.get('GITLAB_USER_AGENT', gitlab.const.USER_AGENT)

DEFAULT_INSTANCE = 'gitlab.com'

ATTACHMENT_RE = re.compile(r'\[(.+?)\]\((/uploads/[^/]+/[^)]+)\)')
ISSUE_URL_RE = re.compile(r'(?:https://[^/]+)?/?(.+?)(?:/-)?/issues/(\d+)')

TIMEOUT = 10

MEMBERS_CACHE = {}

FlatMember = namedtuple('FlatMember', 'member source_path')


class GitLabHelper:
    """Convenience methods for python-gitlab.

    See:
    - https://python-gitlab.readthedocs.io/
    - https://docs.gitlab.com/ee/api/api_resources.html
    """

    def __init__(self, instance=DEFAULT_INSTANCE, api_token=None):
        if api_token is None:
            if instance == DEFAULT_INSTANCE:
                variable = 'GITLAB_API_TOKEN'
            else:
                variable = 'GITLAB_API_TOKEN_' + instance.replace('.', '_')
            api_token = os.environ.get(variable)
        if api_token is None:
            raise Exception('Missing GitLab API token.')
        self.url = 'https://' + instance
        self.api_token = api_token
        self.client = gitlab.Gitlab(self.url, private_token=api_token, user_agent=GITLAB_USER_AGENT)

    @property
    def current_user(self):
        """Return the current user."""

        if not hasattr(self.client, 'user') or self.client.user is None:
            self.client.auth()
        return self.client.user

    def create_issue(self, project, title, **kwargs):
        """Return the new issue."""
        prj = self.client.projects.get(project, lazy=True)
        data = {'title': title}
        data.update(kwargs)
        return prj.issues.create(data)

    # TODO Change return value to label object?
    def ensure_label(self, parent, name, description='', color='#6699cc'):
        """Create the label if necessary and return it as a dict."""
        try:
            label = parent.labels.get(name)
        except GitlabGetError:
            data = {
                'name': name,
                'description': description,
                'color': color,
            }
            label = parent.labels.create(data)
        return label.asdict()

    def find_issues(self, project, state='opened', scope='all', **kwargs):
        """Iterate over matching issues."""
        prj = self.client.projects.get(project, lazy=True)
        params = {
            'state': state,
            'scope': scope,
            'as_list': False,
        }
        params.update(kwargs)
        return prj.issues.list(**params)

    def find_user(self, query):
        """Return the single user with matching username/email/ID, or None."""
        result = None
        if isinstance(query, int):
            try:
                result = self.client.users.get(query)
            except gitlab.exceptions.GitlabGetError as e:
                if e.response_code != 404:
                    raise e
        elif query and '@' not in query:
            if len(users := self.client.users.list(username=query)) == 1:
                result = users[0]
        elif query:
            if len(users := self.client.users.list(search=query)) == 1:
                result = users[0]
        return result

    def get_group(self, group):
        """Return the group, or None."""
        try:
            return self.client.groups.get(group)
        except gitlab.exceptions.GitlabGetError as e:
            if e.response_code != 404:
                raise e
            return None

    def get_issue(self, url=None, project=None, issue=None):
        """Return the issue, or None."""
        result = None
        if url and (m := ISSUE_URL_RE.match(url)):
            project = m.group(1)
            issue = m.group(2)
        if project and issue:
            prj = self.client.projects.get(project, lazy=True)
            try:
                result = prj.issues.get(issue)
            except gitlab.exceptions.GitlabGetError as e:
                if e.response_code != 404:
                    raise e
        return result

    def get_issue_template(self, project, template, ref='main'):
        """Return the issue template."""
        prj = self.client.projects.get(project, lazy=True)
        path = f'.gitlab/issue_templates/{template}'
        file = prj.files.get(file_path=path, ref=ref)
        return str(file.decode(), 'UTF-8')

    def get_project(self, project):
        """Return the project, or None."""
        try:
            return self.client.projects.get(project)
        except gitlab.exceptions.GitlabGetError as e:
            if e.response_code != 404:
                raise e
            return None

    @staticmethod
    def label_age(issue, label):
        """Return the age of the label in days, or None."""
        age = None
        added_at = GitLabHelper.label_datetime(issue, label)
        if added_at:
            now = dt.datetime.now(dt.timezone.utc)
            delta = now - added_at
            age = int(delta.total_seconds() / 86400)
        return age

    @staticmethod
    def label_datetime(issue, label):
        """Return the datetime of the label addition, or None."""
        added_at = None
        for event in issue.resourcelabelevents.list(iterator=True):
            if event.label is None or event.label['name'] != label:
                continue
            if event.action == 'add':
                added_at = dp.isoparse(event.created_at)
            elif event.action == 'remove':
                added_at = None
        return added_at

    @staticmethod
    def parse_attachments(text):
        """Return a dict of title and attachment path in the text."""
        matches = ATTACHMENT_RE.findall(text)
        return {m[0]: m[1] for m in matches}

    @staticmethod
    def parse_emojis(resource, user_id=None):
        """Return a dict of emoji name and count.

        If user_id is not None, count only emojis from that user.
        """
        result = defaultdict(int)
        emojis = resource.awardemojis.list(as_list=False)
        for emoji in emojis:
            if user_id and emoji.user['id'] != user_id:
                continue
            result[emoji.name] = result[emoji.name] + 1
        return result

    @staticmethod
    def _get_breadcrumb_urls(url):
        '''
        Returns the so-called breadcrumbs for a given url of the form
        '<scheme>://<hostname>/<path>' a breadcrumb is any version of
        the provided URL that remains once a path is split
        by '/' and the right part of the split removed.
        '''
        parsed_url = urlparse(url)
        breadcrumb_urls = []

        url_prefix = f'{parsed_url.scheme}://{parsed_url.hostname}'
        path_fragments = parsed_url.path.split('/')
        path_fragments = path_fragments[1:-1]

        for e in path_fragments:
            nurl = f'{url_prefix}/{e}'
            breadcrumb_urls.append(nurl)
            url_prefix = nurl

        return sorted(breadcrumb_urls)


class GitLabAdminHelper(GitLabHelper):
    """Convenience methods for admin-level operations.

    See:
    - https://python-gitlab.readthedocs.io/
    - https://docs.gitlab.com/ee/api/api_resources.html
    """

    def __init__(self, instance=DEFAULT_INSTANCE, admin_token=None):
        if admin_token is None:
            if instance == DEFAULT_INSTANCE:
                variable = 'GITLAB_ADMIN_TOKEN'
            else:
                variable = 'GITLAB_ADMIN_TOKEN_' + instance.replace('.', '_')
            admin_token = os.environ.get(variable)
        if admin_token is None:
            raise Exception('Missing GitLab admin token.')
        super().__init__(instance, api_token=admin_token)

    def get_personal_access_tokens(self, user_id):
        """Return the user's PATs."""
        return self.client.personal_access_tokens.list(user_id=user_id)

    def get_users(self, admins=False, group=None):
        """Return user accounts."""
        if group:
            source = self._users_for_members(group)
        else:
            source = self.client.users.list(admins=admins, as_list=False)
        for u in source:
            if admins and not u.is_admin:
                continue
            yield u

    def _users_for_members(self, group):
        """Return user accounts for members of the group."""
        grp = self.get_group(group)
        if grp is None:
            raise Exception(f'Missing group: {group}')
        members = grp.members.list(all=True, as_list=False)
        for m in filter(lambda m: m.state == 'active', members):
            yield self.client.users.get(m.id)

    def _flatten_members_map(
        self,
        original: dict,
        include_all_access_levels: bool,
        flattened: list = None,
        source_path: list = None,
        override_access: int = 0,
    ):
        '''
        Converts a Members Map as returned by get_members_map() into
        a list of (member, source_path) tuples, taking into consideration
        a uniqueness principle.

        Callers of this method can set its logging level as usual with
        logging.getLogger('autohelp.gitlab').setLevel(logging.DEBUG)
        '''

        if flattened is None:
            flattened = []

        if source_path is None:
            source_path = []
            original = copy.deepcopy(original)
        else:
            source_path = copy.deepcopy(source_path)

        uniqueness_attributes = ['username']

        if include_all_access_levels:
            #   if the user wants all accesses a member has, we calculate the
            #   membership's footprint based on the username, access_level and
            #   the source URL found in the members map returned by
            #   get_memberships_map()

            uniqueness_attributes.append('access_level')

        def footprint_function(e):
            footprint = []
            for a in uniqueness_attributes:
                footprint.append(getattr(e.member, a))
            if include_all_access_levels:
                footprint.append(e.source_path)
            return footprint

        flat_footprint = [footprint_function(e) for e in flattened]

        # the presence of 'shared_with_access' in the membership mapping
        # indicates the users in current resource hierarchy being flattened
        # should have their access level overriden with the one shared

        # only adopt override_access if there's no previous shared_with_access
        # coming from a higher level
        if 'shared_with_access' in original and override_access == 0:
            override_access = original['shared_with_access']

        for e in original.get('direct', []):
            if override_access:
                e.access_level = min(e.access_level, override_access)

            new_flat_member = FlatMember(
                e, ['DIRECT'] if len(source_path) == 0 else source_path
            )

            # for each direct membership in the provided original membersMap
            # we add it to the result if its uniqueness footprint can't be
            # found in the flattened result calculated so far

            f = footprint_function(new_flat_member)
            fi = flat_footprint.index(f) if f in flat_footprint else None

            # if the membership already exists as per its uniqueness
            # footprint only the membership with the highest access_level
            # is kept

            if fi is not None:
                fm = flattened[fi]
                if e.access_level > fm.member.access_level:
                    source_path_string = '>'.join(new_flat_member.source_path)
                    log.debug(f' found {e.username} at {fi}')
                    log.debug(f' has {e.access_level}')
                    log.debug(f' via {source_path_string}')

                    fisource_path_string = '>'.join(fm.source_path)
                    log.debug(f' replacing {fm.member.username}')
                    log.debug(f' which has {fm.member.access_level}')
                    log.debug(f' via {fisource_path_string}')
                    flattened[fi] = new_flat_member
            else:
                flattened.append(new_flat_member)

        for isource_url, imembers in original.items():
            # 'direct' and 'shared_with_access' keys do not require
            # further flattening

            if type(imembers) is dict:
                prefix = ''
                if imembers.get('shared_with_access'):
                    prefix = 'WAS SHARED WITH'
                else:
                    prefix = 'INHERITS FROM'

                new_source_path = source_path + [f'{prefix}>{isource_url}']
                self._flatten_members_map(
                    imembers,
                    include_all_access_levels,
                    flattened,
                    new_source_path,
                    override_access,
                )

        return flattened

    def get_resource_url(self, resource):
        curl = self.client.url

        if isinstance(resource, Group):
            rpath = resource.full_path
        elif isinstance(resource, Project):
            rpath = resource.path_with_namespace

        resource_url = f'{curl}/{rpath}'
        return resource_url

    def get_project_or_group(self, path: str):
        resource = None
        resource = self.get_project(path) or self.get_group(path)
        if resource is None:
            raise GitlabGetError('Group or Project not Found', 404)

        return resource

    def get_members_map(self, resource):
        '''
        Gather the members with access to a given GitLab group or project
        recursively, retrieving first the members with direct access, then
        those with access via ancestor paths or breadcrumbs and finally
        those with access via groups the resource has been shared with.

        This method deliberately calculates permissions relying solely
        on inheritance, share and direct membership relationships,
        thus being independent of the API's calculations and providing a way
        to continuously verify them.

        Callers of this method can set its logging level as usual with
        logging.getLogger('autohelp.gitlab').setLevel(logging.DEBUG)
        '''

        members = {}

        result_in_cache = None
        curl = self.client.url

        if isinstance(resource, Group) or isinstance(resource, Project):
            resource_url = self.get_resource_url(resource)
        elif type(resource) is str:
            resource_url = resource
        else:
            raise Exception('Invalid Resource Type')

        result_in_cache = MEMBERS_CACHE.get(resource_url)

        if result_in_cache:
            members = copy.deepcopy(result_in_cache)
            resource = result_in_cache['object']
        else:
            if type(resource) is str:
                parsed_url = urlparse(resource_url)
                resource_path = parsed_url.path[1:]
                resource = self.get_project_or_group(resource_path)
                resource_url = self.get_resource_url(resource)

            MEMBERS_CACHE[resource_url] = members
            members['object'] = resource

        log.debug(f' gathering members for {resource_url}')

        if result_in_cache:
            log.debug(f' members found in CACHE for {resource_url}')
        else:
            members['direct'] = list(
                resource.members.list(all=True, iterator=True, per_page=100)
            )

            count = len(members['direct'])
            log.debug(
                f'members for {resource_url} not found in CACHE, retrieved {count} direct members'  # noqa
            )

        # As mentioned in https://docs.gitlab.com/ee/user/project/members/
        # share_project_with_groups.html When sharing a project with a group,
        # a user’s assigned Max role is the lowest of either the 'role assigned
        # in the group membership' or the 'maximum role selected when sharing
        # the project with the group'

        breadcrumbs_urls = GitLabHelper._get_breadcrumb_urls(resource_url)
        log.debug(f' {resource_url} has breadcrumbs {breadcrumbs_urls}')

        for burl in breadcrumbs_urls:
            surl, mburl = self.get_members_map(burl)
            members[surl] = mburl

        if not result_in_cache:
            shared_refs = resource.shared_with_groups
            shared_groups = []
            for e in shared_refs:
                ngroup = self.client.groups.get(e['group_id'])
                naccess = e['group_access_level']
                shared_groups.append((ngroup, naccess))

            # for a resource A, ignore groups with which A has been shared that
            # are further down its own hierarchy such as A/B/C. The meaning of
            # 'share A with A/B/C' is undefined since anyone in A/B/C will
            # automatically have access to A and adding it to shared_groups
            # leads to a circular path.

            shared_groups_no_loops = []
            for e, a in shared_groups:
                if not e.web_url.startswith(resource.web_url + '/'):
                    shared_groups_no_loops.append((e, a))

            shared_urls = []
            for e, a in shared_groups:
                nurl = e.web_url.replace(self.client.url + '/groups', curl)
                shared_urls.append((nurl, a, e))

        else:
            # we remove the shared_with_access key as it only applies to the
            # context where the resource was first found and saved in the
            # cache the relevant shared_with_access value will be added later
            # while processing the shared_urls

            if 'shared_with_access' in members:
                members.pop('shared_with_access')

            # if resource is found in the cache, we can use it to calculate
            # the urls of the groups with which the resource has been shared
            # simply by looking at the dicts (sub-members) in the membership
            # map which aren't there because of inheritance but being shared
            # (contain the 'shared_with_access' key)

            shared_urls = [
                (k, members[k]['shared_with_access'], members[k]['object'])
                for k, v in members.items()
                if type(v) is dict and 'shared_with_access' in members[k]
            ]

        log.debug(f' {resource_url} shared with {shared_urls}')

        for group_url, access, group in shared_urls:
            group_url, groupMembers = self.get_members_map(group)
            members[group_url] = groupMembers
            members[group_url]['shared_with_access'] = access
            members[group_url]['object'] = group

        MEMBERS_CACHE[resource_url] = members

        return resource_url, members

    def get_resource_access_paths(self, resource, include_all_access_levels):
        '''
        gathers members and their access paths to a given gitlab
        resource recursively exploring each level of all
        hierarchies and groups with which said resource has
        been shared.

        Callers of this method can set its logging level as usual with
        logging.getLogger('autohelp.gitlab').setLevel(logging.DEBUG)
        '''

        url, mmap = self.get_members_map(resource)
        mlist = self._flatten_members_map(mmap, include_all_access_levels)
        return mlist


class GitLabGraphQLAdminHelper:
    """
    Experimental GitLab GraphQL Helper
    """

    def __init__(
            self,
            instance: str = DEFAULT_INSTANCE,
            admin_token: str = None
    ):
        if admin_token is None:
            if instance == DEFAULT_INSTANCE:
                variable = 'GITLAB_ADMIN_TOKEN'
            else:
                variable = 'GITLAB_ADMIN_TOKEN_' + instance.replace('.', '_')
            admin_token = os.environ.get(variable)
        if admin_token is None:
            raise Exception('Missing GitLab admin token.')

        self.url = 'https://' + instance
        self.api_token = admin_token

        transport = RequestsHTTPTransport(
            url=f'{self.url}/api/graphql',
            headers={"Authorization": f"Bearer {self.api_token}"},
        )

        self.client = Client(
                                transport=transport,
                                fetch_schema_from_transport=True
                            )

    def get_cannonical_username(self, username: str) -> str:
        """
        For a given username, remove its known pre- and suffixes for
        administrative accounts on GitLab.com
        """

        admin_suffixes = ['-admin', '_admin', '\\.admin', 'admin$']  # noqa
        return re.sub('|'.join(admin_suffixes), '', username)

    def get_cannonical_weburl(self, username: str) -> str:
        """
        Returns the web_url for a username on _DEFAULT_GITLAB_INSTANCE

        Args:
            username (str): _description_
        """
        return f'{self.url}/{self.get_cannonical_username(username)}'

    def _query_with_pagination(
        self, query: DocumentNode, variables: dict, page_info_path: tuple
    ) -> dict:
        page_info = {'hasNextPage': True, 'endCursor': None}

        while page_info['hasNextPage']:
            variables.update({'cursor': page_info['endCursor']})
            result = self.client.execute(query, variable_values=variables)
            page_info = reduce(dict.get, page_info_path, result)

            yield result

    def get_users_info(self, usernames: list[str]) -> dict[str, tuple]:
        """
        retrieves users' names, email adresses and their confirmation date
        mapping them to the users' web_url and returning the corresponding
        dictionary

        Args:
            usernames list[str]: list of usernames for which GitLab
            user data should be retrieved

        Returns: a dictionary of {web_url:(name, [email, ...])}
        """

        query = gql(
            """
        query getUOI ($uoi: [String!], $cursor: String){
            users(usernames: $uoi, after: $cursor) {
                pageInfo {
                    endCursor
                    startCursor
                    hasNextPage
                }
                nodes {
                    username,
                    name,
                    webUrl
                    emails {
                        edges {
                            node {
                                id
                                confirmedAt
                                email
                            }
                        }
                    }
                }
            }
        }
        """
        )

        result = {}
        for page in self._query_with_pagination(
            query, {"uoi": usernames}, ('users', 'pageInfo')
        ):
            users = page['users']['nodes']
            for u in users:
                locator = u['webUrl']
                uname = u['name']
                nodes = [n['node'] for n in u['emails']['edges']]
                emails = [n['email'] for n in nodes if n['confirmedAt']]
                result[locator] = (uname, emails)

        return result
