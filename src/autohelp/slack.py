import logging
import os

import requests
import slack_sdk


RESPONSE_TIMEOUT = (10, 60)  # (connect timeout, read timeout)


class SlackHelper:
    """Convenience methods for slack-sdk.

    See:
    - https://slack.dev/python-slack-sdk/
    - https://api.slack.com/bot-users#methods
    """

    def __init__(self, api_token=None):
        api_token = os.environ.get('SLACK_API_TOKEN', api_token)
        if api_token is None:
            raise Exception('Slack API token missing.')
        self.api_token = api_token
        self.client = slack_sdk.WebClient(api_token)

    def find_user(self, email=None):
        """Return the matching User, or None."""
        try:
            res = self.client.users_lookupByEmail(email=email)
            return User(res.data['user'])
        except slack_sdk.errors.SlackApiError as e:
            error = e.response.get('error')
            if error == 'users_not_found':
                return None
            else:
                raise e

    def list_users(self):
        """Iterate over all Users."""
        res = self.client.users_list()
        members = res['members']
        cursor = res['response_metadata'].get('next_cursor')
        while members or cursor:
            if members:
                yield User(members.pop(0))
            else:
                res = self.client.users_list(cursor=cursor)
                members = res['members']
                cursor = res['response_metadata'].get('next_cursor')

    def send_message(
            self,
            target_id,
            text,
            unfurl_links='true',
            unfurl_media='true'):
        """Send a message to the target ID."""
        self.client.chat_postMessage(
            channel=target_id,
            text=text,
            unfurl_links=unfurl_links,
            unfurl_media=unfurl_media,
            as_user='true')


class User:
    """Slack user information.

    See: https://api.slack.com/types/user
    """

    def __init__(self, data):
        self.data = data
        self.profile = data['profile']

    def __repr__(self):
        return f'<User {self.id} ({self.name})>'

    @property
    def name(self):
        """Return a name. (Should not be persisted.)"""
        if (display_name := self.profile.get('display_name')):
            return display_name
        if (real_name := self.profile.get('real_name')):
            return real_name
        return self.data.get('name', 'Unnamed')

    @property
    def id(self):
        """Return the Slack user ID."""
        return self.data.get('id')

    @property
    def team_id(self):
        """Return the Slack team ID."""
        return self.data.get('team_id')

    @property
    def email(self):
        """Return the email address, or None."""
        return self.profile.get('email')


def send_response(response_url, message, **kwargs):
    """Send the message to the Slack response_url."""
    if response_url:
        data = {'text': message}
        data.update(kwargs)
        requests.post(response_url, json=data, timeout=RESPONSE_TIMEOUT)
    else:
        logging.info(f'send_response: {message}')
