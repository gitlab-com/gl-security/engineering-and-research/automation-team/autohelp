from functools import lru_cache
import os

from fastapi import Body, Header, FastAPI, HTTPException, Request
from fastapi.routing import APIRoute
from google.cloud import secretmanager

from autohelp import validate_slack_signature


def check_api_token(request: Request):
    """Verify that the Authorization header contains a valid API token.

    A "Bearer " prefix will be ignored. If the environment variable DEVELOPMENT
    has the value "true", the Authorization header may be empty.

    Usage in FastAPI applications:
    https://fastapi.tiangolo.com/tutorial/dependencies/dependencies-in-path-operation-decorators/

    Raises an HTTPException if the API token is invalid.
    """
    authorization = request.headers.get('authorization')
    if authorization is not None:
        if authorization.startswith('Bearer '):
            authorization = authorization[7:]
        api_tokens = _load_api_tokens()
        for k, v in api_tokens.items():
            if v == authorization:
                break
        else:
            raise HTTPException(status_code=401)
    elif os.environ.get('DEVELOPMENT') == 'true':
        pass
    else:
        raise HTTPException(status_code=401)


@lru_cache
def _load_api_tokens():
    """Return valid API tokens from a GCP Secret Manager secret.

    The environment variable API_TOKENS must contain the resource ID of a
    secret version, prefixed with "gcp:".

    The secret should contain one token (NAME=VALUE) per line:

    ```
    example-token-19700101=0123456789abcdef
    ```

    If the environment variable DEVELOPMENT has the value "true", API_TOKENS
    may be empty.
    """
    name = os.environ.get('API_TOKENS', '')
    if name.startswith('gcp:'):
        name = name[4:]
        client = secretmanager.SecretManagerServiceClient()
        response = client.access_secret_version(name=name, timeout=10)
        content = response.payload.data.decode('utf-8')
    elif os.environ.get('DEVELOPMENT') == 'true':
        return {}
    else:
        raise Exception('Invalid API_TOKENS value.')
    api_tokens = {}
    for line in content.splitlines():
        parts = line.split('=', maxsplit=1)
        if len(parts) == 2 and parts[1]:
            api_tokens[parts[0]] = parts[1]
    return api_tokens


def check_secure_request(request: Request):
    """Verify that the request was made using HTTPS.

    Usage in FastAPI applications:
    https://fastapi.tiangolo.com/tutorial/dependencies/dependencies-in-path-operation-decorators/

    Raises an HTTPException if the request did not use HTTPS.
    """
    if request.url.scheme == 'https':
        pass
    elif os.environ.get('DEVELOPMENT') == 'true':
        pass
    else:
        raise HTTPException(status_code=400, detail='Insecure Request')


# https://fastapi.tiangolo.com/advanced/path-operation-advanced-configuration/
def simplify_operation_ids(app: FastAPI) -> None:
    """Use function names as operation IDs.

    Should be called only after all routes have been added.
    """
    for route in app.routes:
        if isinstance(route, APIRoute):
            route.operation_id = route.name


def valid_slack_request(
        x_slack_signature: str = Header(None),
        x_slack_request_timestamp: int = Header(None),
        body: str = Body(...)):
    """Verify the Slack request signature.

    The environment variable SLACK_SIGNING_SECRET contains the signing secret.

    Always succeeds if the environment variable DEVELOPMENT is set to 'true'.

    More information:
    https://api.slack.com/authentication/verifying-requests-from-slack

    Usage in FastAPI applications:
    https://fastapi.tiangolo.com/tutorial/dependencies/dependencies-in-path-operation-decorators/
    """
    if os.environ.get('DEVELOPMENT') == 'true':
        pass
    else:
        signing_secret = os.environ.get('SLACK_SIGNING_SECRET')
        try:
            validate_slack_signature(
                signing_secret,
                x_slack_signature,
                x_slack_request_timestamp,
                body)
        except Exception as e:
            raise HTTPException(status_code=401)
