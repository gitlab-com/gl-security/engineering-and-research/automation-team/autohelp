import json
import logging
import os

from google.cloud import logging as cloud_logging


LOG_SEVERITY = {
    logging.CRITICAL: 'CRITICAL',
    logging.ERROR: 'ERROR',
    logging.WARNING: 'WARNING',
    logging.INFO: 'INFO',
    logging.DEBUG: 'DEBUG',
}


class LoggingHelper():
    """Helper for using Cloud Logging from CI pipelines."""

    def __init__(self, console=print):
        if 'GOOGLE_CLOUD_LOGGER' in os.environ:
            client = cloud_logging.Client()
            self.logger = client.logger(os.environ['GOOGLE_CLOUD_LOGGER'])
            self.console = console
        else:
            self.logger = None
            self.console = console

    def struct(self, data, level=logging.INFO):
        """Log the given dictionary."""
        severity = LOG_SEVERITY.get(level, 'INFO')
        if self.logger:
            self.logger.log_struct(data, severity=severity)
        if self.console:
            self.console(f'{severity:<8} {json.dumps(data)}')

    def text(self, text, level=logging.INFO):
        """Log the given message."""
        severity = LOG_SEVERITY.get(level, 'INFO')
        if self.logger:
            self.logger.log_text(text, severity=severity)
        if self.console:
            self.console(f'{severity:<8} {text}')
