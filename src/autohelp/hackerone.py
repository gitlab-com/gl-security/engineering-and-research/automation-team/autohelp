import os
import re

import dateutil.parser as dp
import requests


API_URL = 'https://api.hackerone.com/v1'
REPORT_URL = 'https://hackerone.com/reports/{}'

REPORT_ID_RE = re.compile(r'.*?(\d+)')

TIMEOUT = 10

_CVSS_METRIC = {
    'attack_vector': 'AV',
    'attack_complexity': 'AC',
    'privileges_required': 'PR',
    'user_interaction': 'UI',
    'scope': 'S',
    'confidentiality': 'C',
    'integrity': 'I',
    'availability': 'A',
}
_CVSS_VALUE = {
    'network': 'N',
    'adjacent': 'A',
    'local': 'L',
    'physical': 'P',
    'low': 'L',
    'high': 'H',
    'none': 'N',
    'required': 'R',
    'unchanged': 'U',
    'changed': 'C',
}


class HackerOneHelper:
    """HackerOne API client.

    See: https://api.hackerone.com/
    """

    def __init__(self, api_id=None, api_token=None):
        api_id = os.environ.get('H1_API_ID', api_id)
        if api_id is None:
            raise Exception('HackerOne API ID missing.')
        api_token = os.environ.get('H1_API_TOKEN', api_token)
        if api_token is None:
            raise Exception('HackerOne API token missing.')
        self.auth = (api_id, api_token)
        self.programs = None

    def add_comment(self, report, message, internal=False):
        """Add a new comment to the report."""
        resource = f'/reports/{report.id}/activities'
        url = API_URL + resource
        json = {
            'data': {
                'type': 'activity-comment',
                'attributes': {
                    'message': message,
                    'internal': internal
                }
            }
        }
        res = requests.post(url, auth=self.auth, json=json, timeout=TIMEOUT)
        res.raise_for_status()

    def find_reports(self, filters):
        """Return the matching reports.

        Please note: the returned reports do not include activities and
        attachments!

        See: https://api.hackerone.com/customer-resources/#reports-get-all-reports
        """  # noqa
        params = {}
        for k, v in filters.items():
            if isinstance(v, list):
                params[f'filter[{k}][]'] = v
            else:
                params[f'filter[{k}]'] = v
        return self._list('/reports', params=params, entity_type=Report)

    def get_report(self, report_id):
        """Return the report (by ID or URL), or None."""
        if isinstance(report_id, str):
            if (m := REPORT_ID_RE.match(report_id)):
                report_id = m.group(1)
            else:
                raise Exception(f'Invalid report ID: {report_id}')
        resource = f'/reports/{report_id}'
        return self._get(resource, entity_type=Report)

    def get_members(self, program_name):
        """Return the program members."""
        self._ensure_programs()
        if (program_id := self.programs.get(program_name)) is None:
            raise Exception(f'Unknown program: {program_name}')
        resource = f'/programs/{program_id}'
        res = self._get(resource)
        if res is None:
            raise Exception(f'Program not found: {program_name}')
        members = res['relationships']['members']['data']
        return [User(m['relationships']['user']['data']) for m in members]

    def update_policy(self, program_name, policy):
        """Update the program policy.

        See: https://api.hackerone.com/customer-resources/#programs-update-policy
        """  # noqa
        self._ensure_programs()
        if (program_id := self.programs.get(program_name)) is None:
            raise Exception(f'Unknown program: {program_name}')
        if not policy:
            raise Exception('Policy is empty.')
        resource = f'/programs/{program_id}/policy'
        url = API_URL + resource
        json = {
            'data': {
                'type': 'program-policy',
                'attributes': {
                    'policy': policy,
                }
            }
        }
        res = requests.put(url, auth=self.auth, json=json, timeout=TIMEOUT)
        res.raise_for_status()

    def update_reference(self, report, reference):
        """Update the report's issue tracker reference."""
        resource = f'/reports/{report.id}/issue_tracker_reference_id'
        url = API_URL + resource
        json = {
            'data': {
                'type': 'issue-tracker-reference-id',
                'attributes': {
                    'reference': reference
                }
            }
        }
        res = requests.post(url, auth=self.auth, json=json, timeout=TIMEOUT)
        res.raise_for_status()

    def _ensure_programs(self):
        """Initializes the programs dict, if necessary."""
        if self.programs is None:
            programs = {}
            for program in self._list('/me/programs'):
                program_id = program['id']
                program_name = program['attributes']['handle']
                programs[program_name] = program_id
            self.programs = programs

    def _get(self, resource='', params=None, entity_type=None):
        """Return the resource, or None."""
        url = API_URL + resource
        res = requests.get(
            url,
            params=params,
            auth=self.auth,
            timeout=TIMEOUT)
        if res.status_code == 200:
            if entity_type is None:
                return res.json().get('data')
            else:
                return entity_type(res.json().get('data'))
        elif res.status_code == 404:
            return None
        else:
            raise HackerOneError(res)

    def _list(self, resource='', params=None, entity_type=None):
        """Iterate over matching resources."""
        url = API_URL + resource
        entities = []
        while url or entities:
            if entities:
                if entity_type is None:
                    yield entities.pop(0)
                else:
                    yield entity_type(entities.pop(0))
            else:
                res = requests.get(
                    url,
                    params=params,
                    auth=self.auth,
                    timeout=TIMEOUT)
                if res.status_code != 200:
                    raise HackerOneError(res)
                json = res.json()
                if 'links' in json:
                    url = json['links'].get('next')
                else:
                    url = None
                params = None
                entities = json.get('data')


class HackerOneError(Exception):
    """HackerOne API error with status code."""

    def __init__(self, response):
        super().__init__(response.text)
        self.status = response.status_code


class HackerOneObject():
    """Base class for HackerOne API objects."""

    def __init__(self, data, object_type, users=None):
        if object_type and data['type'] != object_type:
            raise Exception(f"Invalid object type: {data['type']}")
        self.data = data
        self.attributes = data.get('attributes', {})
        if users is not None:
            self.users = users
        else:
            self.users = {}
            relationships = data.get('relationships', {})
            HackerOneObject._collect_users(relationships, self.users)

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.id}>'

    @property
    def id(self):
        """Return the object ID."""
        return self.data['id']

    @property
    def type(self):
        """Return the object type."""
        return self.data['type']

    def get(self, key, default=None):
        """Return the attribute, or the default."""
        return self.attributes.get(key, default)

    def relationship(self, key, default=None):
        """Return the relationship's data, or the default."""
        if key in self.data['relationships']:
            return self.data['relationships'][key]['data']
        else:
            return default

    @staticmethod
    def _collect_users(relationships, users):
        """Add all Users under relationships to users, by ID."""
        for k, v in relationships.items():
            data = v.get('data', [])
            if isinstance(data, dict):
                data = [data]
            if not isinstance(data, list):
                continue
            for item in data:
                if item.get('type') == 'user':
                    user = User(item)
                    users[str(user.id)] = user
                else:
                    nested = item.get('relationships', {})
                    HackerOneObject._collect_users(nested, users)


class Activity(HackerOneObject):
    """HackerOne Activity.

    See: https://api.hackerone.com/customer-reference/#activity
    """

    def __init__(self, data):
        super().__init__(data, None)

    def __repr__(self):
        return f'<Activity {self.id} ({self.type})>'

    @property
    def attachments(self):
        """Return the Attachments."""
        data = self.relationship('attachments', [])
        return [Attachment(d) for d in data]

    @property
    def actor(self):
        """Return the authoring User, or None."""
        data = self.relationship('actor')
        if data and data['type'] == 'user':
            return User(data)
        else:
            return None

    @property
    def created_at(self):
        """Return the "created_at" datetime."""
        return dp.parse(self.get('created_at'))

    @property
    def internal(self):
        """Return the "internal" attribute."""
        return self.get('internal')

    @property
    def message(self):
        """Return the "message" attribute."""
        return self.get('message')


class Attachment(HackerOneObject):
    """HackerOne Attachment.

    See: https://api.hackerone.com/customer-reference/#attachment
    """

    def __init__(self, data):
        super().__init__(data, 'attachment')

    def __repr__(self):
        return f'<Attachment {self.id} ({self.file_name})>'

    @property
    def content_type(self):
        """Return the "content_type" attribute."""
        return self.get('content_type')

    @property
    def expiring_url(self):
        """Return the "expiring_url" attribute."""
        return self.get('expiring_url')

    @property
    def file_name(self):
        """Return the "file_name" attribute."""
        return self.get('file_name')


class Group(HackerOneObject):
    """HackerOne Group.

    See: https://api.hackerone.com/customer-reference/#group
    """

    def __init__(self, data):
        super().__init__(data, 'group')

    def __repr__(self):
        return f'<Group {self.id} ({self.name})>'

    @property
    def is_user(self):
        return False

    @property
    def name(self):
        """Return the "name" attribute."""
        return self.get('name')

    @property
    def permissions(self):
        """Return the "permissions" attribute."""
        return self.get('permissions')


class Report(HackerOneObject):
    """HackerOne Report.

    See: https://api.hackerone.com/customer-reference/#report
    """

    def __init__(self, data):
        super().__init__(data, 'report')

    @property
    def activities(self):
        """Return the Activities."""
        data = self.relationship('activities', [])
        return [Activity(d) for d in data]

    @property
    def assignee(self):
        """Return the assigned User/Group, or None."""
        data = self.relationship('assignee')
        if data and data['type'] == 'user':
            return User(data)
        elif data and data['type'] == 'group':
            return Group(data)
        else:
            return None

    @property
    def assignee_username(self):
        """Return the assigned User's username, or None."""
        data = self.relationship('assignee')
        if data and data['type'] == 'user':
            return data['attributes']['username']
        else:
            return None

    @property
    def attachments(self):
        """Return the Attachments."""
        data = self.relationship('attachments', [])
        return [Attachment(d) for d in data]

    @property
    def bounty_total(self):
        """Return the sum of all bounties."""
        result = 0.0
        bounties = self.relationship('bounties', [])
        for b in bounties:
            result += float(b['attributes']['amount'])
            result += float(b['attributes']['bonus_amount'])
        return result

    @property
    def created_at(self):
        """Return the creation datetime."""
        return dp.parse(self.get('created_at'))

    @property
    def description(self):
        """Return the description."""
        return self.get('vulnerability_information')

    @property
    def disclosed_at(self):
        """Return the disclosed_at datetime, or None."""
        data = self.get('disclosed_at')
        if data:
            return dp.parse(data)
        else:
            return None

    @property
    def public_activity_at(self):
        """Return the last public activity datetime, or None."""
        data = self.get('last_public_activity_at')
        if data:
            return dp.parse(data)
        else:
            return None

    @property
    def reporter(self):
        """Return the reporting User."""
        return User(self.relationship('reporter'))

    @property
    def severity(self):
        """Return the severity, or None."""
        data = self.relationship('severity')
        if data:
            return Severity(data, users=self.users)
        else:
            return None

    @property
    def state(self):
        """Return the "state" attribute."""
        return self.get('state')

    @property
    def suggested_bounty(self):
        """Return the most recent suggested bounty, or 0.0."""
        result = 0.0
        bounties = list(filter(
            lambda a: a.type == 'activity-bounty-suggested', self.activities))
        if bounties:
            last = bounties[-1]
            result += float(last.get('bounty_amount'))
            result += float(last.get('bonus_amount'))
        return result

    @property
    def title(self):
        """Return the "title" attribute."""
        return self.get('title')

    @property
    def tracker_id(self):
        """Return the issue tracker reference ID, or None."""
        return self.get('issue_tracker_reference_id')

    @property
    def tracker_url(self):
        """Return the issue tracker reference URL, or None."""
        return self.get('issue_tracker_reference_url')

    @property
    def triaged_at(self):
        """Return the triage datetime, or None."""
        data = self.get('triaged_at')
        if data:
            return dp.parse(data)
        else:
            return None

    @property
    def weakness_id(self):
        """Return the weakness ID, or None."""
        data = self.relationship('weakness')
        if data:
            return data['attributes']['external_id']
        else:
            return None

    @property
    def weakness_name(self):
        """Return the weakness name, or None."""
        data = self.relationship('weakness')
        if data:
            return data['attributes']['name']
        else:
            return None

    @property
    def web_url(self):
        """Return the report's browser URL."""
        return REPORT_URL.format(self.id)


class Severity(HackerOneObject):
    """HackerOne Severity.

    See: https://api.hackerone.com/customer-reference/#severity
    """

    def __init__(self, data, users):
        super().__init__(data, 'severity', users=users)

    @property
    def cvss_score(self):
        """Return the CVSS score, or None."""
        return self.get('score')

    @property
    def cvss_vector(self):
        """Return the CVSS vector (without version prefix), or None."""
        cvss_metrics = []
        for h1_key, cvss_key in _CVSS_METRIC.items():
            h1_value = self.get(h1_key)
            cvss_value = _CVSS_VALUE.get(h1_value)
            if cvss_value is not None:
                cvss_metrics.append(f'{cvss_key}:{cvss_value}')
        return '/'.join(cvss_metrics)

    @property
    def rating(self):
        """Return the severity rating.

        See: https://api.hackerone.com/customer-reference/#severity-ratings
        """
        return self.get('rating')

    @property
    def user(self):
        """Return the User, or None."""
        return self.users.get(self.user_id)

    @property
    def user_id(self):
        """Return the user ID as a string."""
        return str(self.get('user_id'))


class User(HackerOneObject):
    """HackerOne User.

    See: https://api.hackerone.com/customer-reference/#user
    """

    def __init__(self, data):
        super().__init__(data, 'user')

    def __repr__(self):
        return f'<User {self.id} ({self.username})>'

    @property
    def is_hackerone(self):
        """Return True if the user is a HackerOne triager."""
        return self.get('hackerone_triager', False)

    @property
    def is_user(self):
        return True

    @property
    def name(self):
        """Return the "name" attribute."""
        return self.get('name')

    @property
    def username(self):
        """Return the "username" attribute."""
        return self.get('username')
