import csv
import datetime as dt
from functools import wraps
import hashlib
import hmac
import time
import yaml

import logging
logging.basicConfig(level=logging.ERROR)

# callers of this package can set the logging level for it
# as usual with logging.getLogger('autohelp').setLevel(logging.DEBUG)

REQUEST_TTL = 300  # Number of seconds that Slack requests are valid


class File:
    """File I/O helpers."""

    @staticmethod
    def read_csv(filename):
        """Read the CSV file as a list of dicts.

        Key names are taken from the first line of the CSV file.
        """
        result = []
        first_row = True
        columns = []
        width = 0
        with open(filename, 'r', newline='', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                if first_row:
                    columns = row
                    width = len(columns)
                    first_row = False
                else:
                    if len(row) != width:
                        msg = f'Invalid column count (expected {width}): {row}'
                        raise Exception(msg)
                    result.append({k: v for k, v in zip(columns, row)})
        return result

    @staticmethod
    def read_yaml(filename):
        """Return the parsed YAML data."""
        with open(filename, 'r') as file:
            return yaml.safe_load(file)

    @staticmethod
    def sha256(filename):
        """Return a string of the file's SHA-256 digest."""
        with open(filename, 'rb') as file:
            m = hashlib.sha256()
            while True:
                b = file.read(65536)
                if not b:
                    break
                m.update(b)
            return m.hexdigest()

    @staticmethod
    def write_csv(filename, columns, rows):
        """Write the column names and row data as a CSV file."""
        width = len(columns)
        with open(filename, 'w', newline='', encoding='utf-8') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(columns)
            for row in rows:
                if len(row) != width:
                    msg = f'Invalid column count (expected {width}): {row}'
                    raise Exception(msg)
                writer.writerow(row)

    @staticmethod
    def write_yaml(filename, data):
        """Write the data as a YAML file."""
        with open(filename, 'w') as file:
            yaml.dump(data, file)


def stopwatch(log=print):
    """Log the number of seconds the decorated function ran for."""

    def decorate(fn):
        @wraps(fn)
        def wrapped(*args, **kwargs):
            time_a = dt.datetime.now()
            result = fn(*args, **kwargs)
            time_b = dt.datetime.now()
            seconds = (time_b - time_a).total_seconds()
            log(f'{fn.__name__}: {seconds:.3f} seconds')
            return result

        return wrapped

    return decorate


def validate_slack_signature(signing_secret, signature, timestamp, body):
    """Validate the Slack request signature."""
    if not signing_secret:
        raise Exception('Missing signing secret')
    if not signature:
        raise Exception('Missing signature')
    if not timestamp:
        raise Exception('Missing timestamp')
    if abs(time.time() - int(timestamp)) > REQUEST_TTL:
        raise Exception('Expired timestamp')
    sig = signature.split('=', 2)
    if len(sig) != 2 or sig[0] != 'v0':
        raise Exception(f'Unsupported signature: {signature}')
    base = f'{sig[0]}:{timestamp}:{body}'
    digest = hmac.new(signing_secret.encode(), base.encode(), 'sha256')
    valid_signature = hmac.compare_digest(sig[1], digest.hexdigest())
    if not valid_signature:
        raise Exception('Invalid signature')
